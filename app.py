from flask import Flask, request, jsonify
import json
import requests
from flask_cors import CORS
from kafka import KafkaConsumer, KafkaProducer
app = Flask(__name__)
TOPIC_NAME = "DATASETS"
KAFKA_SERVER = "localhost:9092"

producer = KafkaProducer(
    bootstrap_servers = KAFKA_SERVER,
    api_version = (0, 11, 15)
)

@app.route('/kafka/pushDataset', methods=['POST'])
def kafkaProducer():
    req = request.get_json()
    print("Entered the function")
    json_payload = json.dumps(req)
    json_payload = str.encode(json_payload)
    print("Sent to kafka")
    producer.send(TOPIC_NAME, json_payload)
    producer.flush()
    print("Sent to consumer")
    #print(req)
    return jsonify({
        "message": "The dataset is started to prepare", 
        "status": "Pass"})

@app.route('/api/sendMetrics', methods=['POST'])
def sendMetrics():
    res = requests.post('http://localhost:8082/updateMetrics', json=request.get_json())
    return res.json()


if __name__ == "__main__":
    app.run(debug=True, port = 5000)